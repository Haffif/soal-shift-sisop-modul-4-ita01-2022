#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <fuse.h>
#include <dirent.h>

static const char *dirpath = "/home/ubuntu/Documents";
char prefix[10] = "Animeku_";
char prefix2[10] = "IAN_";
char prefix3[12] = "nam_do-saq_";
char key[15] = "INNUGANTENG";

void enkripsi1(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;

  char a[10];
  char new[1000];
  int totalN = 0, flag = 0, ext = 0;
  memset(a, 0, sizeof(a));
  memset(new, 0, sizeof(new));

  for (int i = 0; i < strlen(string); i++)
  {
    if (string[i] == '/')
      continue;
    if (string[i] == '.')
    {
      new[totalN++] = string[i];
      flag = 1;
    }
    if (flag == 1)
      a[ext++] = string[i];
    else
      new[totalN++] = string[i];
  }

  for (int i = 0; i < totalN; i++)
  {
    if (isupper(new[i])) //uppercase maka atbash cipher
      new[i] = 'A' + 'Z' - new[i];
    else if (islower(new[i])) //lowercase maka rot13 cipher
    {
      if (new[i] > 109) // huruf n keatas dikurang 13
        new[i] -= 13; // krn jika ditambah akan melebihi z
      else
        new[i] += 13; // huruf a s/d m ditambah 13 seperti biasa
    }
  }

  strcat(new, a);
  strcpy(string, new);
}

void logged(char *command, char *msg, char *old, char *new)
{
  FILE *fptr;
  fptr = fopen("Wibu.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Failed in opening file]");
    exit(1);
  }
  fprintf(fptr, "%s\t%s\t%s\t-->\t%s\n", command, msg, old, new);
  fclose(fptr);
}

void enskripsi2(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;
  int mark = 0;
  for (int i = 0; i < strlen(string); i++)
  {
    if (key[mark] == '\0')
      mark = 0;
    if (isupper(string[i]))
      string[i] = (string[i] - 'A' + key[mark++] - 'A') % 26 + 'A';
    else if (islower(string[i]))
      string[i] = (string[i] - 'a' + tolower(key[mark++]) - 'a') % 26 + 'a';
    else
      mark = 0;
  }
}

void decode2(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;
  int mark = 0;
  for (int i = 0; i < strlen(string); i++)
  {
    if (key[mark] == '\0')
      mark = 0;
    if (isupper(string[i]))
      string[i] = (string[i] - 'A' - key[mark++] + 'A' + 26) % 26 + 'A';
    else if (islower(string[i]))
      string[i] = (string[i] - 'a' - tolower(key[mark++]) + 'a' + 26) % 26 + 'a';
    else
      mark = 0;
  }
}

void logged2(char *command, char *old, char *new)
{
  time_t times;
  struct tm *timeinfo;
  time(&times);
  timeinfo = localtime(&times);

  FILE *fptr;
  fptr = fopen("/home/ubuntu/hayolongapain_ITA01.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Failed in opening file]");
    exit(1);
  }
  else
  {
    if (strcmp(command, "RENAME") == 0)
      fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    else if (strcmp(command, "RMDIR") == 0 || strcmp(command, "UNLINK") == 0)
      fprintf(fptr, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    else
      fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
  }
  fclose(fptr);
}

void revstr(char *str1)
{
  int i, len, temp;
  len = strlen(str1);
  for (i = 0; i < len / 2; i++)
  {
    temp = str1[i];
    str1[i] = str1[len - i - 1];
    str1[len - i - 1] = temp;
  }
}

void getSpecialFile(char *file)
{
  if (!strcmp(file, ".") || !strcmp(file, ".."))
    return;

  char ext[1000], binary[1000], newFile[1000];
  memset(ext, 0, sizeof(ext));
  memset(binary, 0, sizeof(binary));
  memset(newFile, 0, sizeof(newFile));
  int nExt = 0, nFile = 0, nBin = 0, flag = 0;
  strcpy(newFile, "");

  for (int i = strlen(file) - 1; i >= 0; i--)
  {
    if (file[i] == '.' && flag == 0)
    {
      ext[nExt++] = file[i];
      flag = 1;
      continue;
    }
    if (flag == 1)
      newFile[nFile++] = file[i];
    else
      ext[nExt++] = file[i];
  }

  revstr(ext);
  revstr(newFile);

  if (!strcmp(newFile, ""))
  {
    memset(newFile, 0, sizeof(newFile));
    strcpy(newFile, ext);
    memset(ext, 0, sizeof(ext));
    nFile = nExt;
    nExt = 0;
  }

  for (int i = 0; i < nFile; i++)
  {
    if (newFile[i] >= 97 && newFile[i] <= 122)
    {
      binary[nBin++] = '1';
      newFile[i] -= 32;
    }
    else
      binary[nBin++] = '0';
  }

  int add = 1, decimalVal = 0;
  char dec[1000];
  memset(dec, 0, sizeof(dec));

  for (int i = nBin - 1; i >= 0; i--)
  {
    if (binary[i] == '1')
      decimalVal += add;
    add *= 2;
  }

  sprintf(dec, "%d", decimalVal);
  strcpy(file, newFile);
  strcat(file, ext);
  strcat(file, ".");
  strcat(file, dec);
}

void getNormalFile(char *file)
{
  if (!strcmp(file, ".") || !strcmp(file, ".."))
    return;

  char finalFile[1000];
  memset(finalFile, 0, sizeof(finalFile));
  char *token = strtok(file, "/");

  while (token != NULL)
  {
    char ext[1000], dec[1000], newFile[1000];
    memset(ext, 0, sizeof(ext));
    memset(dec, 0, sizeof(dec));
    memset(newFile, 0, sizeof(newFile));
    int nExt = 0, nFile = 0, nDec = 0, flag = 0;

    for (int i = strlen(token) - 1; i >= 0; i--)
    {
      if (token[i] == '.' && flag == 0)
      {
        flag = 1;
        continue;
      }
      if (token[i] == '.' && flag == 1)
      {
        ext[nExt++] = token[i];
        flag = 2;
        continue;
      }
      if (flag == 1)
        ext[nExt++] = token[i];
      else if (flag == 2)
        newFile[nFile++] = token[i];
      else
        dec[nDec++] = token[i];
    }

    revstr(dec);
    revstr(ext);
    revstr(newFile);

    if (!strcmp(newFile, ""))
    {
      strcpy(newFile, ext);
      memset(ext, 0, sizeof(ext));
    }

    int decVal = atoi(dec);
    int nBin = 0;
    char binary[1000];
    memset(binary, 0, sizeof(binary));

    while (decVal > 0)
    {
      binary[nBin++] = (char)(48 + decVal % 2);
      decVal /= 2;
    }

    revstr(binary);

    for (int i = 0; i < nBin; i++)
    {
      if (binary[i] == '1')
        newFile[i] += 32;
    }

    strcat(newFile, ext);
    strcat(finalFile, "/");
    strcat(finalFile, newFile);
    token = strtok(NULL, "/");
  }
  strcpy(file, finalFile);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;
  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
      continue;
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (str != NULL)
      enkripsi1(de->d_name);
    if (str2 != NULL)
      enskripsi2(de->d_name);
    if (str3 != NULL)
      getSpecialFile(de->d_name);

    res = (filler(buf, de->d_name, &st, 0));
    if (res != 0)
      break;
  }

  logged2("READDIR", fpath, "");

  closedir(dp);
  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char *str = strstr(path, prefix);

  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int fd = 0;
  (void)fi;
  fd = open(fpath, O_RDONLY);

  if (fd == -1)
    return -errno;

  res = pread(fd, buf, size, offset);
  if (res == -1)
    res = -errno;

  logged2("READ", fpath, "");

  close(fd);
  return res;
}

static int xmp_rename(const char *source, const char *destination)
{
  int res;
  char fpath_source[1000];
  char fpath_destination[1000];
  char *p_fpath_source, *p_fpath_destination;

  if (strcmp(source, "/") == 0)
  {
    source = dirpath;
    sprintf(fpath_source, "%s", source);
  }
  else
    sprintf(fpath_source, "%s%s", dirpath, source);

  if (strcmp(source, "/") == 0)
    sprintf(fpath_destination, "%s", dirpath);
  else
    sprintf(fpath_destination, "%s%s", dirpath, destination);

  res = rename(fpath_source, fpath_destination);
  if (res == -1)
    return -errno;

  p_fpath_source = strrchr(fpath_source, '/');
  p_fpath_destination = strrchr(fpath_destination, '/');
  if (strstr(p_fpath_source, "Animeku_"))
    logged("RENAME", "terdecode", fpath_source, fpath_destination);
  if (strstr(p_fpath_destination, "Animeku_"))
    logged("RENAME", "terenkripsi", fpath_source, fpath_destination);

  logged2("RENAME", fpath_source, fpath_destination);

  return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int fd;
  (void)fi;

  fd = open(fpath, O_WRONLY);
  if (fd == -1)
    return -errno;

  res = pwrite(fd, buf, size, offset);
  if (res == -1)
    res = -errno;

  logged2("WRITE", fpath, "");

  close(fd);
  return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = open(fpath, fi->flags);
  if (res == -1)
    res = -errno;

  logged2("OPEN", fpath, "");

  close(res);
  return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = mkdir(fpath, mode);
  if (res == -1)
    res = -errno;

  logged2("MKDIR", fpath, "");

  close(res);
  return 0;
}

static int xmp_rmdir(const char *path)
{
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = rmdir(fpath);
  if (res == -1)
    res = -errno;

  logged2("RMDIR", fpath, "");

  close(res);
  return 0;
}

static int xmp_unlink(const char *path)
{
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = unlink(fpath);
  if (res == -1)
    res = -errno;

  logged2("UNLINK", fpath, "");

  close(res);
  return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = creat(fpath, mode);
  if (res == -1)
    res = -errno;

  logged2("CREATE", fpath, "");

  close(res);
  return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = truncate(fpath, size);
  if (res == -1)
    res = -errno;

  logged2("TRUNCATE", fpath, "");

  close(res);
  return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .write = xmp_write,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .create = xmp_create,
    .truncate = xmp_truncate,
};

int main(int argc, char *argv[])
{
  umask(0);

  return fuse_main(argc, argv, &xmp_oper, NULL);
}