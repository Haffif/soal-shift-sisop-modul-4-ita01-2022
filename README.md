# soal-shift-sisop-modul-4-ITA01-2022

Hasil Pengerjaan Soal Shift Praktikum SISTEM OPERASI 2022<br>
Anggota Kelompok:<br>
<ol>
<li>Haffif Rasya Fauzi (5027201002)</li>
<li>Muhammad Faris Anwari (5027201008)</li>
<li>Muhammad Dzakwan (5027201065)</li>
</ol>

---
## Soal 1
(Revisi soal nomor 1)

Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
<ol type="a">
    <li>Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13</li>
    <li>Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.</li>
    <li>Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.</li>
    <li>Setiap data yang terencode akan masuk dalam file “Wibu.log”</li>
    <li>Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)</li>
</ol>

Catatan:
filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode

### Penyelesaian

Kode:<br>
[Source code](./anya_ITA01.c)

#### 1.a Encode File Dengan Atbash Cipher dan Rot13
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13. untuk kodenya yaitu sebagai berikut:

```c#
void enkripsi1(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;

  char a[10];
  char new[1000];
  int totalN = 0, flag = 0, ext = 0;
  memset(a, 0, sizeof(a));
  memset(new, 0, sizeof(new));

  for (int i = 0; i < strlen(string); i++)
  {
    if (string[i] == '/')
      continue;
    if (string[i] == '.')
    {
      new[totalN++] = string[i];
      flag = 1;
    }
    if (flag == 1)
      a[ext++] = string[i];
    else
      new[totalN++] = string[i];
  }

  for (int i = 0; i < totalN; i++)
  {
    if (isupper(new[i]))
      new[i] = 'A' + 'Z' - new[i];
    else if (islower(new[i]))
    {
      if (new[i] > 109)
        new[i] -= 13;
      else
        new[i] += 13;
    }
  }

  strcat(new, a);
  strcpy(string, new);
}
```

Sesuai dengan source code di atas, encode dilakukan dengan pembuatan function `enkripsi1(char *string)`. Function `enkripsi1(char *string)` akan memiliki argumen `char*string` yang merupakan nama file atau folder yang akan diencode. Pada function `enkripsi1(char *string)` akan diperiksa terlebih dahulu apakah `file` berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka encode tidak perlu dilakukan. kemudian, dibuat variabel `a` untuk menampung extension suatu file dan `new` untuk menampung nama file atau folder yang akan diencode(tanpa extension) yang masing-masing variabel bertipe string. Masing-masing variabel akan dilakukan `memset` yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output. Variabel lain yang dibuat yaitu ext yang akan memiliki panjang dari extension, `totalN` yang akan memiliki panjang dari nama file, dan `flag` yang digunakan sebagai penanda antara nama file dan extensionnya yang semua variabel tersebut diset memiliki nilai 0.

pada tahap selanjutnya akan dicari nama file atau folder beserta extensionnya jika ada yaitu dengan melakukan perulangan sebanyak `strlen(string)` atau panjang dari file. Pada perulangan, jika `string[i]` merupakan `.`, maka pada new akan ditambahkan `.` dan `flag` akan berubah nilainya menjadi 1 yang berarti sudah masuk pada fase extension. Namun, jika `string[i]` bukan merupakan `.`, maka akan diperiksa terlebih dahulu `flag` bernilai 0 atau 1, jika bernilai 0, maka `string[i]` akan ditambahkan pada `new`, jika sebaliknya, maka `string[i]` akan ditambahkan pada `a`. `totalN` akan bertambah ketika `new` bertambah dan `ext` akan bertambah ketika `a` bertambah. Setelah itu, `new` akan diencode dengan perulangan sebanyak `totalN`. Pada encode yang dilakukan, akan diperiksa apakah `new[i]` adalah uppercase atau lowercase. Jika merupakan uppercase, maka yang akan dilakukan adalah encode dengan atbash cipher yaitu menggunakan formula `'A' +'z' - new[i]`, tetapi jika merupakan lowercase, maka yang akan dilakukan adalah encode dengan rot13 yaitu penambahan 13 atau pengurangan 13. Pada encode dengan rot13, jika `newFile` merupakan karakter `a` hingga `m` maka akan dilakukan penambahan 13, tetapi jika new merupakan karakter `n` hingga `z` maka akan dilakukan pengurangan 13. Lalu, `new` dan `a` akan digabungkan menggunakan `strcat()` dan hasilnya akan dicopy pada `string` menggunakan `strcpy()`.

#### 1.b dan 1.c Rename file kemudian encode atau decode
Untuk menyelesaikan 1.b dan 1.c maka akan digunakan code sebagai berikut:

```c#
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};
```

```c#
static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}
```

```c#
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;
  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
      continue;
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (str != NULL)
      enkripsi1(de->d_name);
    if (str2 != NULL)
      enskripsi2(de->d_name);
    if (str3 != NULL)
      getSpecialFile(de->d_name);

    res = (filler(buf, de->d_name, &st, 0));
    if (res != 0)
      break;
  }

  logged2("READDIR", fpath, "");

  closedir(dp);
  return 0;
}
```

```c#
static int xmp_rename(const char *source, const char *destination)
{
  int res;
  char fpath_source[1000];
  char fpath_destination[1000];
  char *p_fpath_source, *p_fpath_destination;

  if (strcmp(source, "/") == 0)
  {
    source = dirpath;
    sprintf(fpath_source, "%s", source);
  }
  else
    sprintf(fpath_source, "%s%s", dirpath, source);

  if (strcmp(source, "/") == 0)
    sprintf(fpath_destination, "%s", dirpath);
  else
    sprintf(fpath_destination, "%s%s", dirpath, destination);

  res = rename(fpath_source, fpath_destination);
  if (res == -1)
    return -errno;

  p_fpath_source = strrchr(fpath_source, '/');
  p_fpath_destination = strrchr(fpath_destination, '/');
  if (strstr(p_fpath_source, "Animeku_"))
    logged("RENAME", "terdecode", fpath_source, fpath_destination);
  if (strstr(p_fpath_destination, "Animeku_"))
    logged("RENAME", "terenkripsi", fpath_source, fpath_destination);

  logged2("RENAME", fpath_source, fpath_destination);

  return 0;
}
```

Source code diatas untuk dapat menyelesaikan permasalahan pada no 1 dibutuhkan operasi getattr, readdir, read, dan operasi rename. Dari source code diatas yang pertama kita lakukan adalah mencari tau apakah nama file tersebut mengandung prefix yang diminta dalam hal soal no 1, prefix yang diminta adalah "Animeku_" maka dapat digunakan metode stl pada string dengan `strstr(path,prefix)` dimana variabel `prefix` merupakan array berisi file "Animeku_". Lalu dicek apakah variabel `str` berisi atau tidak, jika tidak berisi maka artinya tidak ditemukan file dengan awalan "Animeku_" sedangkan jika bertemu maka diartikan bahwa ditemukan file yang berawalan "Animeku_". Kemudian, kita mau memindahkan kursor address dari index ke - 0 menjadi nama file atau folder pertama setelah prefix folder, maka kita akan menambahkan kursor tersebut sebanyak panjang prefix yang diminta lalu tambahkan method stl `strchr(str,'/') `yang artinya itu menemukan folder setelah folder dengan awalan prefix. Langkah terakhir, dari string tersebut akan dienkripsikan atau didecode menggunakan function `enkripsi1(temp)` hal ini dilakukan karena decode dan encode untuk no 1 sama persis.

#### 1.d Menulis Log Rename pada "Wibu.log"
Setiap data yang terencode akan masuk dalam file “Wibu.log”. untuk code-nya yaitu sebagai berikut:

```c#
void logged(char *command, char *msg, char *old, char *new)
{
  FILE *fptr;
  fptr = fopen("Wibu.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Gagal dalam membuka file]");
    exit(1);
  }
  fprintf(fptr, "%s\t%s\t%s\t-->\t%s\n", command, msg, old, new);
  fclose(fptr);
}
```

Dari source code diatas yang pertama kita lakukan adalah membuka file log tersebut yaitu "Wibu.log" dengan mode "a" yaitu append. Lalu dicek apakah file berhasil dibuka atau tidak, jika tidak maka outputkan errorrnya dan melakukan `exit(1)`. Jika berhasil maka printf kan ke dalam file tersebut sesuai dengan format yang ada dalam soal ke-1 dan tutup file jika sudah tidak digunakan lagi. Pencatatan log akan dilakukan pada rename yaitu di method `xmp_rename` dengan melakukan pengecekan pada variabel `p_fpath_source` atau `p_fpath_destination` apakah mengandung kata prefix yang diminta, jika p_fpath_source mengandung kata prefix yang diminta artinya file tersebut akan di decode sedangkan jika `p_fpath_destination` yang tidak kosong alias terdapat prefix yang diminta artinya file tersebut akan di enkripsikan.

### Output
- Direktori "Animeku_" sebelum encoding:

![sebelum encoding](./img/animeku_sblm.JPG)

- Direktori "Animeku_" sesudah encoding:

![sesudah encoding](./img/animeku_ssdh.JPG)

---
## Soal 2
(Revisi soal nomor 2)

Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :
<ol type="a">
    <li>Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).</li>
    <li>Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.</li>
    <li>Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.</li>
    <li>Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.</li>
    <li>Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut :</li>
</ol>

`[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]`

Keterangan:
```
Level : Level logging
dd : Tanggal
mm : Bulan
yyyy : Tahun
HH : Jam (dengan format 24 Jam)
MM : Menit
SS : Detik
CMD : System call yang terpanggil
DESC : Informasi dan parameter tambahan
```

Contoh :<br>
WARNING::21042022-12:02:42::RMDIR::/RichBrian<br>
INFO::21042022-12:01:20::RENAME::/Song::/IAN_Song<br>
INFO::21042022-12:06:41::MKDIR::/BillieEilish<br>

```
NOTE :
Misalkan terdapat file bernama idontwannabeyouanymore.txt pada direktori
BillieEilish.
“IAN_Song/BillieEilish/idontwannabeyouanymore.txt” →
“IAN_Song/JvyfoeRbpvyp/qqbhzwngrnhmlbognlfsek.bkg”
Referensi : https://www.dcode.fr/vigenere-cipher
```

### Penyelesaian

Kode:<br>
[Source code](./anya_ITA01.c)

#### 2.a Encode File dengan Viginere Cipher
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere)

```c#
void enskripsi2(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;
  int mark = 0;
  for (int i = 0; i < strlen(string); i++)
  {
    if (key[mark] == '\0')
      mark = 0;
    if (isupper(string[i]))
      string[i] = (string[i] - 'A' + key[mark++] - 'A') % 26 + 'A';
    else if (islower(string[i]))
      string[i] = (string[i] - 'a' + tolower(key[mark++]) - 'a') % 26 + 'a';
    else
      mark = 0;
  }
}
```

```c#
void decode2(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;
  int mark = 0;
  for (int i = 0; i < strlen(string); i++)
  {
    if (key[mark] == '\0')
      mark = 0;
    if (isupper(string[i]))
      string[i] = (string[i] - 'A' - key[mark++] + 'A' + 26) % 26 + 'A';
    else if (islower(string[i]))
      string[i] = (string[i] - 'a' - tolower(key[mark++]) + 'a' + 26) % 26 + 'a';
    else
      mark = 0;
  }
}
```

Sesuai dengan source code di atas, encode dilakukan dengan pembuatan function `enskripsi2(char *string)`. Function `enskripsi2(char *string)` akan memiliki argumen `char *string` yang merupakan nama file atau folder yang akan diencode. Sedangkan function `void decode2(char *string)` akan memiliki argumen `char *string` yang merupakan nama file atau folder yang akan didecode.

Pada function `enskripsi2(char *string)` akan diperiksa terlebih dahulu apakah file berisi `.` atau `..`, jika merupakan salah satu dari pilihan tersebut, maka encode tidak perlu dilakukan.
Setelah itu, string akan diencode dengan perulangan sebanyak `strlen(string)`. Pada encode yang dilakukan, akan diperiksa apakah `string[i]` adalah *uppercase* atau *lowercase* serta apakah key[mark] == '\0. Jika merupakan uppercase, maka yang akan dilakukan adalah encode dengan Vigenere Cipher yaitu menggunakan formula `string[i] = (string[i] - 'A' + key[mark++] - 'A') % 26 + 'A'`, tetapi jika merupakan lowercase, maka formula yang akan digunakan adalah `string[i] = (string[i] - 'a' + tolower(key[mark++]) - 'a') % 26 + 'a'`.
Jika string akan didecode, akan dilakukan hal yang sama sesuai penjelasana no 2 hanya saja ada perbedaan formula, yaitu jika `string[i]` merupakan uppercase akan digunakkan formula `string[i] = (string[i] - 'A' - key[mark++] + 'A' + 26) % 26 + 'A'` sedangkan jika string[i] merupakan lowercase akan digunakkan formula `string[i] = (string[i] - 'a' - tolower(key[mark++]) + 'a' + 26) % 26 + 'a'`

#### 2.b dan 2.c Rename file kemudian encode atau decode

Jika suatu direktori di rename dengan `“IAN_[nama]”`, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a. Apabila nama direktori dihilangkan `“IAN_”`, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

```c#
static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }

  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }

  char *str3 = strstr(path, prefix3);
  if (str3 != NULL)
  {
    str3 += strlen(prefix3);
    char *temp = strchr(str3, '/');
    if (temp != NULL)
      getNormalFile(temp);
  }

  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}
```

```c#
  char *str2 = strstr(path, prefix2);
  if (str2 != NULL)
  {
    str2 += strlen(prefix2);
    char *temp = strchr(str2, '/');
    if (temp != NULL)
    {
      temp += 1;
      decode2(temp);
    }
  }
```

- Source code diatas untuk dapat menyelesaikan permasalahan pada no 1 dibutuhkan operasi getattr, readdir, read, rename, rmdir, mkdir, unlink, link, open, dan write.
- Dari source code diatas yang pertama kita lakukan adalah mencari tau apakah nama file tersebut mengandung prefix yang diminta dalam hal soal no 2, prefix yang diminta adalah "IAN_", maka dapat digunakan method stl pada string dengan strstr(path, prefix2) dimana variabel prefix2 merupakan array yang berisi "IAN_".
- Lalu dicek apakah variabel str2 berisi atau tidak, jika tidak berisi maka artinya tidak ditemukan file dengan awalan "IAN_" sedangkan jika bertemu maka diartikan bahwa ditemukan file yang berawalan "IAN_".
- Lalu kita mau memindahkan kursor address dari index ke - 0 menjadi nama file atau folder pertama setelah prefix folder, maka kita akan menambahkan kursor tersebut sebanyak panjang prefix yang diminta lalu tambahkan method stl strchr(str2,'/') yang artinya itu menemukan folder setelah folder dengan awalan prefix.
- Lalu sisa dari string tersebut akan di dienkripsikan atau didecode menggunakan function enkripsi2(temp) untuk encode sedangkan untuk decode digunakkan fungsi decode2(temp).

#### 2.d dan 2.e Menulis Log

```c#
void logged2(char *command, char *old, char *new)
{
  time_t times;
  struct tm *timeinfo;
  time(&times);
  timeinfo = localtime(&times);

  FILE *fptr;
  fptr = fopen("/home/ubuntu/hayolongapain_ITA01.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Gagal dalam membuka file]");
    exit(1);
  }
  else
  {
    if (strcmp(command, "RENAME") == 0)
      fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    else if (strcmp(command, "RMDIR") == 0 || strcmp(command, "UNLINK") == 0)
      fprintf(fptr, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
    else
      fprintf(fptr, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
              timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, command, old, new);
  }
  fclose(fptr);
}
```

- Dari source code diatas yang pertama kita lakukan adalah membuka file log tersebut yaitu "/home/ubuntu/hayolongapain_ITA01.log" dengan mode "a" yaitu append .
- Lalu dicek apakah file berhasil dibuka atau tidak, jika tidak maka outputkan errorrnya dan melakukan exit(1).
- Jika berhasil maka printf kan ke dalam file tersebut sesuai dengan format yang ada dalam soal ke-2.
- Tutup file jika sudah tidak digunakkan lagi.
- Pencatatan log akan dilakukan pada setiap fungsi xmp yang ada seperti pada xmp_read, xmp_readdir, xmp_mkdir, dan lainnya.

### Output
- Direktori "IAN_" sebelum encoding:

![sebelum encoding](./img/ian_sblm.JPG)

- Direktori "IAN_" sesudah encoding:

![sesudah encoding](./img/ian_ssdh.JPG)
